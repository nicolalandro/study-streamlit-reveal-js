# Study Streamlit Reveal JS
This repo is a study to how to include revealjs presentation on streamlit.

!["screen show the result"](imgs/screen.png)

## How to run

```
docker-compose up 
# go to localhost:8000
```

# References
* python
* docker, docker compose
* streamlit
* [streamlit reveal-js](https://github.com/bouzidanas/streamlit-reveal-slides)

