# Reveal.js + Streamlit
Add <a target="_blank" href="https://revealjs.com/">Reveal.js</a> presentations to your Streamlit app.

Note:
this is a note

-----

## First slide
You can use code:

```
pip install streamlit-reveal-slides
```

And you can cite:

\[[GitHub](https://github.com/bouzidanas/streamlit.io/tree/7748c2a97f4ca54ce4b8120a054d6c66e8be296d/streamlit-reveal-slides)\] \[[PyPI](https://pypi.org/project/streamlit-reveal-slides/)\]

Note:
this is a note

---

## Another example
Here

Note:
this is a note

-----
## The end

Note:
this is a note