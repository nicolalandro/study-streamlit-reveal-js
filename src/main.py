import streamlit as st
import reveal_slides as rs

st.title("RevealJS Slide")
st.text("F to full screan")
st.text("Ctrl+shift+F to search")
st.text("S for speacker view popup")

with open('src/slide.md', 'r') as f:
    sample_markdown = f.read()

currState = rs.slides(
    sample_markdown,
    height=400, 
    theme="dracula",
    markdown_props={
         "data-separator":"^-----",
         "data-separator-vertical":"^---",
         "data-separator-notes":"^Note:"
    },
    config={
        "transition": "convex",
        "plugins": ["search", "notes"],
    }
)

st.text(f"The slide state is: {currState}.")
